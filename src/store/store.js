import Vue from "vue";

export const store = Vue.observable({
    count: 27,
    workData: null,
    skillsData: null,
    loggedIn: false,
    paramId: null,
    isReady: false,
    codes: ['unddog', 'port12']
});

export const mutations = {
    setCount(count) {
        store.count = count;
    },
    setWorkData(json) {
        store.workData = json;
    },
    setSkillsData(json) {
      store.skillsData = json;
    },
    setLoggedIn(bool) {
        store.loggedIn = bool;
    },
    setParamId(id) {
        store.paramId = id;
    },
    setIsReady(bool) {
        store.isReady = bool
    }
};

export const actions = {
    getWorkData() {
        return new Promise((resolve, reject) => {
            Vue.axios.get('http://admin.derrickypenburg.com/wp-json/wp/v2/posts?categories=2&per_page=100')
                .then((response) => {
                    let resp = response.data.map(x => x.acf);
                    mutations.setWorkData(resp)
                    resolve(resp);
                }).catch((error) => {
                    reject(error)
                })
            });
    },
    getSkillsData() {
        return new Promise((resolve, reject) => {
            Vue.axios.get('http://admin.derrickypenburg.com/wp-json/wp/v2/pages/174')
                .then((response) => {
                    // console.log('skills data loaded: ', response);
                    let resp = {
                        content: response.data.content.rendered,
                        skills: response.data.acf.add_new_skill//content: response.data.
                    }
                    mutations.setSkillsData(resp)
                    resolve(resp);
                }).catch((error) => {
                reject(error)
            })
        });
    }
};
