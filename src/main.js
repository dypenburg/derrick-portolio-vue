import Vue from 'vue'
import VueRouter from 'vue-router'
import VueMeta from 'vue-meta'
import VueAnalytics from 'vue-ua'
import vuetify from '@/plugins/vuetify'
import vueHeadful from 'vue-headful';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueSimpleAlert from "vue-simple-alert";

import App from './App.vue'

Vue.use(VueRouter)
Vue.use(VueMeta, { refreshOnceOnNavigation: true })
Vue.use(VueAnalytics, {
  appName: 'derrickypenburg.com',
  appVersion: '1.0',
  trackingId: 'UA-173541960-1',
  vueRouter: router,
})
Vue.use(vueHeadful)
axios.defaults.withCredentials = false;
Vue.use(VueAxios, axios);
Vue.use(VueSimpleAlert);

Vue.config.productionTip = false
Vue.config.devtools = process.env.NODE_ENV === 'development'

function lazyLoad(view) {
  return() => import(`@/sections/${view}.vue`)
}

const routes = [
  { path: '/', name:'main', component: lazyLoad('Main') },
  { path: '/about', name:'about', component: lazyLoad('About') },
  { path: '/contact', name:'contact', component: lazyLoad('Contact') }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
